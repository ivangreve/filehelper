﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileHelper
{
    public class FieldOrderAttribute : Attribute
    {
        public int Order { get; set; }
    }
}
