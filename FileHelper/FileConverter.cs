﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FileHelper
{
    public class FileConverter
    {
        private string Extension;
        private List<object> _itemList;
        private string _itemSeparator;
        private StringBuilder _stringBuilder = new StringBuilder();

        public FileConverter(List<object> itemList)
        {
            Extension = ".txt";
            _itemSeparator = ",";
            _itemList = itemList;
        }

        public FileConverter(string extension, string itemSeparator, List<object> itemList)
        {
            Extension = extension;
            _itemSeparator = itemSeparator;
            _itemList = itemList;
        }

        public string ConvertToFile()
        {
            if (!_itemList.Any()) return string.Empty;
            _stringBuilder.Clear();

            var lstProps = GetPropertiesList(_itemList.First());
            foreach (var objDto in _itemList)
            {
                //var lstParams = GetParameterList(objDto); 
                var lineString = ProcessObjToStringLine(objDto, lstProps);
                _stringBuilder.AppendLine(lineString);
            }
            return _stringBuilder.ToString();
        }

        private string ProcessObjToStringLine(object objDto, PropertyInfo[] lstProps)
        {
            var index = 1;
            var lineString = string.Empty;
            foreach (var property in lstProps)
            {
                var value = GetPropValue(objDto, property.Name);
                lineString = string.Concat(lineString, value);

                var isLastProperty = index == lstProps.Length;

                if (isLastProperty)
                    return lineString;
                else
                    lineString = string.Concat(lineString, _itemSeparator);

                index++;
            }
            return string.Empty;
        }

        private static PropertyInfo[] GetPropertiesList(object objDto)
        {
            var lstParams = objDto.GetType().GetProperties()
                .Select(x => new
                {
                    Property = x,
                    Attribute = (FieldOrderAttribute)Attribute.GetCustomAttribute(x, typeof(FieldOrderAttribute), true)
                }).OrderBy(x => x.Attribute != null ? x.Attribute.Order : -1)
                .Select(x => x.Property)
                .ToArray();

            return lstParams;
        }

        private static object GetPropValue(object src, string propName)
        {
            var type = src.GetType();
            var property = type.GetProperty(propName);
            return property.GetValue(src);
        }
    }
}
