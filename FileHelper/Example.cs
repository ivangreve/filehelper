﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileHelper
{
    public class Example
    {
        //Respetar orden de properties para un correcto ordenamiento en reflection
        [FieldOrderAttribute(Order = 1)]
        public string Campo1 { get; set; }

        [FieldOrderAttribute(Order = 2)]
        public string Campo2 { get; set; }

        [FieldOrderAttribute(Order = 3)]
        public string Campo3 { get; set; }

        [FieldOrderAttribute(Order = 4)]
        public string Campo4 { get; set; }

        [FieldOrderAttribute(Order = 5)]
        public string Campo5 { get; set; }

        [FieldOrderAttribute(Order = 6)]
        public string Campo6 { get; set; }

    }
}
